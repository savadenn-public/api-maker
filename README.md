# API maker
Under development 🚧

This tool allows to easily create a ready-to-code API project using the latest versions of PHP, Symfony & API-Platform.


## Usage

```bash
docker run --rm --interactive --tty  --volume $PWD:/app registry.gitlab.com/savadenn-public/api-maker
```

Answer "yes" to Flex recipes.

Then, you're ready to code!

## Manually build & push image

```bash
docker build -t registry.gitlab.com/savadenn-public/api-maker .
docker push registry.gitlab.com/savadenn-public/api-maker
```