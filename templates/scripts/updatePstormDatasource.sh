#!/usr/bin/env bash

set -u
set -e

DB_PHPSTORM=.idea/dataSources.xml

# $1 Service name
function updateDBaccess {
  hash=$(docker-compose ps -q "$1")
  echo $hash
  IP=$(docker inspect -f \
    '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' \
    "${hash}")
  POSTGRES_PASSWORD=$(docker exec "${hash}" /bin/sh -c 'echo "${POSTGRES_PASSWORD}"')
  POSTGRES_USER=$(docker exec "${hash}" /bin/sh -c 'echo "${POSTGRES_USER}"')
  POSTGRES_DB=$(docker exec "${hash}" /bin/sh -c 'echo "${POSTGRES_DB}"')

  if [ -f "${DB_PHPSTORM}" ]; then
  	echo "Updating dataSources"
  	sed -i -E "s#jdbc:postgresql://(.*):#jdbc:postgresql://${IP}:#" "${DB_PHPSTORM}"
  fi
}

updateDBaccess database
