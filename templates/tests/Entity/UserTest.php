<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

/**
 * Test class of User
 */
class UserTest extends TestCase
{
    /**
     * Test User::getRoles()
     */
    public function testRoles(): void
    {
        $user = new User('guid');

        $this->assertEquals(['ROLE_USER'], $user->getRoles());

        // Four times same role
        $user->setRoles(['ROLE_FOO', 'ROLE_FOO', 'ROLE_FOO', 'ROLE_FOO']);
        // Expect only unique roles
        $expected = [
            0 => 'ROLE_FOO',
            4 => 'ROLE_USER',
        ];
        $this->assertEquals($expected, $user->getRoles());
    }

    /**
     * Test creating user from an empty payload
     */
    public function testCreateFromEmptyPayload(): void
    {
        $expected = new User('dummy_guid');
        $actual   = User::createFromPayload('dummy_guid', []);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Test creating user from payload
     */
    public function testCreateFromPayload(): void
    {
        $expected = new User('dummy_guid');
        $expected->setRoles(['ROLE_FOO']);

        $actual = User::createFromPayload('dummy_guid', ['roles' => ['ROLE_FOO']]);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Test methods supposed to return user identifier
     */
    public function testIdentifier(): void
    {
        $expected = 'foo';
        $user     = new User($expected);

        $this->assertEquals($expected, $user->getUserIdentifier());
        $this->assertEquals($expected, $user->getGuid());
        $this->assertEquals($expected, $user->getUsername());
    }
}
