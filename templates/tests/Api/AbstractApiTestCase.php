<?php

declare(strict_types=1);

namespace App\Tests\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

/**
 * Class AbstractApiTestCase
 */
abstract class AbstractApiTestCase extends ApiTestCase
{
    /**
     * Create a http client with a valid Authorization header
     *
     * @param array $roles User roles
     *
     * @return Client
     */
    protected function createAuthenticatedClient(array $roles = ['ROLE_USER']): Client
    {
        $manager = static::getContainer()->get(JWTTokenManagerInterface::class);
        $user    = User::createFromPayload('0322b1fa-d41b-4a86-9d66-ff3db220c701', ['roles' => $roles]);
        $jwt     = $manager->create($user);

        return static::createClient([], ['headers' => ['authorization' => 'Bearer '.$jwt]]);
    }
}
