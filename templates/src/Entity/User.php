<?php

declare(strict_types=1);

namespace App\Entity;

use Lexik\Bundle\JWTAuthenticationBundle\Security\User\JWTUserInterface;

/**
 * Class User
 */
class User implements JWTUserInterface
{
    private string $guid;

    private array $roles = [];

    public function __construct(string $guid)
    {
        $this->guid = $guid;
    }

    public static function createFromPayload($username, array $payload): self
    {
        $user = new self($username);

        return $user->setRoles($payload['roles'] ?? []);
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function getUserIdentifier(): string
    {
        return $this->guid;
    }

    public function getUsername(): string
    {
        return $this->guid;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function eraseCredentials(): void
    {
        // Nothing to do. Leave blank
    }
}
