# Technical stack

This project is built with API platform. 

* [Read the official "Getting Started" guide](https://api-platform.com/docs/distribution).

## App database
The app database is based on [PostgreSQL](https://www.postgresql.org/) v13.

::: note
See service `database` in `docker-compose.yaml`.
:::

## Authentication
Authentication is based on [Json Web Token](https://jwt.io/) for several reasons:

* No database table is needed
* Used across services
* Proven technology
* Lots of library available

The JWT is provided by service [Authentication](https://gitlab.com/savadenn-public/workshop-tools/authentication/).
