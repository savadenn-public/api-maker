# Profiling with blackfire.io
This project uses [blackfire](https://blackfire.io/) to monitor performances.

It implies creating an account on https://blackfire.io/. This will lead you getting [credentials](https://blackfire.io/my/settings/credentials).

To enable profiling, fill up file `.env` with your server credentials:

```dotenv
BLACKFIRE_SERVER_ID=xxx-xxx-xxx-xxx-xxx
BLACKFIRE_SERVER_TOKEN=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

::: warning
Be careful to NOT commit it.
:::

To profile requests using CLI, you have to open a shell in the Blackfire container, then run CLI command with your client credentials:
```bash
docker-compose exec blackfire sh
blackfire run bin/console --client-id=xxx-xxx-xxx-xxx-xxx --client-token=xxx
```