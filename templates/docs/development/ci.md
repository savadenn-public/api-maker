# Running CI locally
Sometimes, you may want to run CI locally, for debug purpose for example.  
First of all, you'll need to [install the `gitlab-runner` client](https://docs.gitlab.com/runner/install/index.html).

As `gitlab-ci.yml` includes parts fo CI from external repositories, you need to get the merged YAML file.  
Go to your project repository, then CI/CD > Editor.  
There, click on tab "View merged YAML":

![View merged YAML](development/merged_gitlabci.png)

Copy the whole YAML and put it in your local `gitlab-ci.yml`.

::: note
Make sure variables `DS_MAJOR_VERSION` and `LICENSE_MANAGEMENT_VERSION` are defined as `string`, note `integer`.
:::

Then, you can run the wanted job:

```bash
gitlab-runner exec docker cve_dependencies_php
```