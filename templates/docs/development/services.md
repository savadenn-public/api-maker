# Services

* API 
    * [Endpoint](https://time-manager.docker.localhost/api)
    * [Swagger UI](https://time-manager.docker.localhost/api/docs)
* Authentication
    * [Endpoint](https://time-manager.docker.localhost/auth)
    * [Swagger UI](https://time-manager.docker.localhost/auth/docs)