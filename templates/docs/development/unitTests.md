# Running unit tests
This project ships with unit tests.

To execute unit tests only, run the following command:

```bash
docker-compose exec php bin/phpunit
```