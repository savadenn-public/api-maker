# Build & push PHP image for CI
Some jobs in `.gitlab-ci.yaml` use a custom image of PHP. To manually build it, run the following commands:
```bash
docker build -t registry.gitlab.com/YOUR_PROJECT_PATH/php8.1-ci:latest ./docker/ci
```

And push it on registry:
```bash
docker push registry.gitlab.com/YOUR_PROJECT_PATH/php8.1-ci:latest
```