FROM php:8.1-alpine

COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN apk update && apk add rsync yq

COPY templates /templates
COPY entrypoint.sh .

CMD ./entrypoint.sh
