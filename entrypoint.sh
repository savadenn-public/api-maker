#!/usr/bin/env sh
set -e

cd /app

# Create fresh new project
composer create-project symfony/skeleton project_name
cd project_name

# Ignore directory of generated documentation
echo "/dist" >> .gitignore

# Install main dependencies
composer require api mercure lexik/jwt-authentication-bundle symfony/translation sentry/sentry-symfony symfony/monolog-bundle symfony/uid

# Install dev dependencies
composer require --dev hautelook/alice-bundle escapestudios/symfony2-coding-standard squizlabs/php_codesniffer symfony/web-profiler-bundle symfony/maker-bundle phpunit/phpunit symfony/test-pack symfony/debug-bundle

rsync -r /templates/ .

# Update/clean-up .env
echo "COMPOSE_PROJECT_NAME=project_name" >> .env
echo "APP_RELEASE=" >> .env
echo "# API Platform distribution
TRUSTED_PROXIES=127.0.0.0/8,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16
TRUSTED_HOSTS=^(localhost|caddy)$" >> .env

sed -i -r 's/^DATABASE_URL=".+"/DATABASE_URL="postgresql:\/\/api-platform:!ChangeMe!@database:5432\/api?serverVersion=13\&charset=utf8"/' .env
sed -i '/^JWT_SECRET_KEY/d' .env
sed -i '/^JWT_PASSPHRASE/d' .env

# Update .env.test
echo "JWT_PUBLIC_KEY=%kernel.project_dir%/config/jwt/public.pem  # Used to validate JWT signature
JWT_SECRET_KEY=%kernel.project_dir%/config/jwt/private.pem # Used to create JWT during functional tests" >> .env.test

echo "# API Platform distribution
TRUSTED_HOSTS=^example.com$
DATABASE_URL=\"postgresql://api-platform:!ChangeMe!@database:5432/api-test?serverVersion=13&charset=utf8\"" >> .env.test

# Packages configuration -----------------------------------------------------------------------------------------------
# Remove prefix /api on routes
sed -i '/prefix: \/api/d' config/routes/api_platform.yaml

# Configure sentry.yaml
yq -I4 -i e '.sentry.register_error_listener |= false' config/packages/sentry.yaml
yq -I4 -i e '.sentry.options.release |= "%env(APP_RELEASE)%"' config/packages/sentry.yaml

# Configure translation.yaml
yq -I4 -i e '.framework.enabled_locales |= ["en", "fr"]' config/packages/translation.yaml

# Configure nelmio_cors.yaml
yq -I4 -i e '.nelmio_cors.defaults.allow_headers += ["Preload", "Fields"]' config/packages/nelmio_cors.yaml

# Configure lexik_jwt_authentication.yaml
yq -I4 -i e '.lexik_jwt_authentication.user_identity_field |= "guid"' config/packages/lexik_jwt_authentication.yaml
yq -I4 -i e 'del(.lexik_jwt_authentication.secret_key)' config/packages/lexik_jwt_authentication.yaml
yq -I4 -i e 'del(.lexik_jwt_authentication.pass_phrase)' config/packages/lexik_jwt_authentication.yaml

# Configure doctrine.yaml
yq -I4 -i e '.doctrine.orm.mappings.App.type |= "attribute"' config/packages/doctrine.yaml

# Configure doctrine.yaml
yq -I4 -i e '.doctrine.orm.mappings.App.type |= "attribute"' config/packages/doctrine.yaml

# Configure framework.yaml
yq -I4 -i e 'del(.framework.session)' config/packages/framework.yaml
yq -I4 -i e '.framework.set_locale_from_accept_language |= true' config/packages/framework.yaml
yq -I4 -i e '.framework.set_content_language_from_locale |= true' config/packages/framework.yaml

exec "$@"
